import java.util.Locale

@Suppress("DSL_SCOPE_VIOLATION") // TODO: Remove once KTIJ-19369 is fixed
plugins {
    alias(libs.plugins.androidLibrary)
    alias(libs.plugins.jetbrainsKotlinAndroid)
//    id("maven-publish")
//    alias(libs.plugins.jetbrainsKotlinAndroidExtensions)
}

android {
    namespace = "cn.kviewui.scancode"
    compileSdk = 34

    defaultConfig {
        minSdk = 21

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    viewBinding {
        enable = true
    }
    libraryVariants.all {
        val variantName = name
        outputs.all {
            if (this is com.android.build.gradle.internal.api.ApkVariantOutputImpl) {
                this.outputFileName = "your_custom_aar_name-$variantName.aar"
            }
        }
    }
}

// 启用 AAR 打包
//afterEvaluate {
//    publishing {
//        publications {
//            create("maven", MavenPublication::class.java) {
//                groupId = "cn.kviewui"
//                artifactId = "scancode"
//                version = "1.0.0"
//                artifact("$buildDir/outputs/aar/${groupId}${artifactId}.aar")
//                pom.withXml {
//                    val dependenciesNode = asNode().appendNode("dependencies")
//                    configurations.getByName("implementation").allDependencies.forEach {
//                        val dependencyNode = dependenciesNode.appendNode("dependency")
//                        dependencyNode.appendNode("groupId", it.group)
//                        dependencyNode.appendNode("artifactId", it.name)
//                        dependencyNode.appendNode("version", it.version)
//                    }
//                }
//            }
//        }
//
//        repositories {
//            maven {
//                url = uri("file://${project.rootDir}/repo")
//            }
//        }
//    }
//}

dependencies {

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.material)
    implementation(libs.barcode.scanning)
    implementation(libs.androidx.camera.core)
    implementation(libs.androidx.camera.camera2)
    implementation(libs.camera.lifecycle)
    implementation(libs.androidx.camera.view)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.junit)
    androidTestImplementation(libs.androidx.espresso.core)
}